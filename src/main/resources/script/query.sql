create table author_tb(
                          author_id serial primary key ,
                          author_name varchar(50) not null,
                          gender varchar(50)  not null
);

create table categories_tb(
                              category_id serial primary key,
                              category_name varchar(255) not null
);

create table book_td(
                        book_id serial primary key ,
                        title varchar(255) not null,
                        published_date timestamp not null,
                        author_id integer not null ,
                        constraint authorId_fk foreign key (author_id) references author_tb
                            on delete cascade on update cascade
);

create table book_detail(
                            id serial primary key,
                            book_id integer not null,
                            category_id integer not null ,
                            constraint bookId_fk foreign key (book_id) references book_td
                                on update cascade on delete cascade,
                            constraint categoryId_fk foreign key (category_id) references categories_tb
                                on update cascade on delete cascade
);
------------------------------------------------------------------------------------------------------
SELECT  * FROM categories_tb where category_id =1;
SELECT  * FROM author_tb where  author_id =1 ;

DELETE FROM author_tb WHERE author_id =4 RETURNING *;
INSERT INTO author_tb(author_name, gender) VALUES ('Kok ALEX','male') RETURNING author_id;


DELETE from categories_tb where category_id =6 ;
INSERT INTO categories_tb(category_name) values ('KDrama')

UPDATE author_tb SET author_name ='koko' ,gender = 'female' WHERE author_id = 1 RETURNING *;

UPDATE categories_tb SET category_name = 'SleepBook' WHERE category_id=12 RETURNING  *;

SELECT * FROM book_td;

SELECT category_name ,ct.category_id FROM book_detail INNER JOIN
    categories_tb ct on book_detail.category_id = ct.category_id WHERE book_id = 1;

INSERT INTO book_td(title,published_date,author_id) VALUES ('koko','2-17-2023',7);

DELETE FROM book_td WHERE book_id = 20;