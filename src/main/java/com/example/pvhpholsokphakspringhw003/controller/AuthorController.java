package com.example.pvhpholsokphakspringhw003.controller;

import com.example.pvhpholsokphakspringhw003.model.entity.Author;
import com.example.pvhpholsokphakspringhw003.model.request.AuthorRequest;
import com.example.pvhpholsokphakspringhw003.model.response.AuthorResponse;
import com.example.pvhpholsokphakspringhw003.service.AuthorService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/authors")
public class AuthorController {
    private final AuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping("get-all-author")
    @Operation(summary = "Get all Author")
    public ResponseEntity<AuthorResponse<List<Author>>> geAllAuthor(){

        AuthorResponse<List<Author>> response = AuthorResponse.<List<Author>>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .message("Get All Author SuccessFully")
                .status(200)
                .payload(authorService.getAllAuthor())
                .build();

        return ResponseEntity.ok().body(response);
    }

    @GetMapping("get-author-by-id/{id}")
    @Operation(summary = "Get author by Id")
    public ResponseEntity<AuthorResponse<Author>> getAuthorById(@PathVariable("id") Integer authorId){
        AuthorResponse<Author> response =AuthorResponse.<Author>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .status(200)
                .message("Get Author by Id Successfully")
                .payload(authorService.getAllAuthorbyId(authorId))
                .build();

        return  ResponseEntity.ok().body(response);
    }

    //delete
    @DeleteMapping("delete-author-by-id/{id}")
    @Operation(summary = "Delete author by Id")
    public  ResponseEntity<AuthorResponse<String>> deleteAuthorById(@PathVariable("id") Integer authorId){
            AuthorResponse<String>  response= null ;
            if (authorService.deleteAuthorById(authorId) == true){
                response =AuthorResponse.<String>builder()
                        .timestamp(new Timestamp(System.currentTimeMillis() ))
                        .status(200)
                        .message("Successfully deleted Author")
                        .build();

            }

        return ResponseEntity.ok().body(response);
    }

    //insert
    @PostMapping("add-new-author")
    @Operation(summary = "Add New Author")
    public ResponseEntity<AuthorResponse<Author>> insertNewAuthor(@RequestBody AuthorRequest authorRequest){
        Integer storeAuthorId = authorService.inseertNewAuthor(authorRequest);
        if (storeAuthorId!= null){
            AuthorResponse<Author> response = AuthorResponse.<Author>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully added new author")
                    .payload(authorService.getAllAuthorbyId(storeAuthorId))
                    .build();
            return ResponseEntity.ok().body(response);
        }
        return null;
    }

    //update
    @PutMapping("update-author-by-id/{id}")
    @Operation(summary = "Update Author by Id")
    public ResponseEntity<AuthorResponse<Author>> updateAuthorById(
            @RequestBody AuthorRequest authorRequest , @PathVariable("id") Integer authorId
    ){
        AuthorResponse<Author> response = null;
        Author idAuthorUpdate = authorService.updateAuthor(authorRequest,authorId);
            if (idAuthorUpdate != null){
                response = AuthorResponse.<Author>builder()
                        .timestamp(new Timestamp(System.currentTimeMillis()))
                        .status(200)
                        .message("Update Author Successfully")
                        .payload(authorService.updateAuthor(authorRequest,authorId))
                        .build();
            }

        return ResponseEntity.ok().body(response);
    }

}
