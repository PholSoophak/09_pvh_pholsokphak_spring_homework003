package com.example.pvhpholsokphakspringhw003.controller;

import com.example.pvhpholsokphakspringhw003.model.entity.Author;
import com.example.pvhpholsokphakspringhw003.model.entity.Category;
import com.example.pvhpholsokphakspringhw003.model.request.AuthorRequest;
import com.example.pvhpholsokphakspringhw003.model.request.CategoryRequest;
import com.example.pvhpholsokphakspringhw003.model.response.AuthorResponse;
import com.example.pvhpholsokphakspringhw003.model.response.CategoryResponse;
import com.example.pvhpholsokphakspringhw003.service.AuthorService;
import com.example.pvhpholsokphakspringhw003.service.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.yaml.snakeyaml.util.EnumUtils;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/categories")
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("get-all-categories")
    @Operation(summary = "Get all Category")
    public ResponseEntity<CategoryResponse<List<Category>>> getAllCategory(){
        CategoryResponse<List<Category>> response = CategoryResponse.<List<Category>>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .status(200)
                .message("Get all Categories Successfully")
                .payload(categoryService.getAllCategory())
                .build();

        return ResponseEntity.ok(response);
    }

    @GetMapping("get-category-by-id/{id}")
    @Operation(summary = "Get Category By Id")
    public ResponseEntity<CategoryResponse<Category>> getCategoryById(@RequestParam("id") Integer categoryId){
        CategoryResponse<Category> response = CategoryResponse.<Category>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .status(200)
                .message("Get Author By Id Successfully")
                .payload(categoryService.getCategoryById(categoryId))
                .build();
        return ResponseEntity.ok().body(response);
    }

    //Delete
    @DeleteMapping("delete-category-by-id/{id}")
    @Operation(summary = "Delete category by Id")
    public ResponseEntity<CategoryResponse<String>> deleteCategoryById(@PathVariable("id") Integer categoryId){
        CategoryResponse<String> response = null;
        if (categoryService.deleteCategoryById(categoryId) == true){
            response = CategoryResponse.<String>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully deleted category")
                    .build();
            return ResponseEntity.ok().body(response);

        }
        return null;
    }

    //insert
    @PostMapping("add-new-category")
    @Operation(summary = "Add new Category")
    public ResponseEntity<CategoryResponse<Category>> insertNewCategory(@RequestBody CategoryRequest categoryRequest){

            CategoryResponse<Category> response = CategoryResponse.<Category>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully added new Category")
                    .payload(categoryService.insertNewCategory(categoryRequest)).build();
            return ResponseEntity.ok().body(response);

    }
    @PutMapping("update-category-by-id/{id}")
    @Operation(summary = "Update Category by Id")
    public ResponseEntity<CategoryResponse<Category>> updateCategoryById(
            @RequestBody CategoryRequest categoryRequest , @PathVariable("id") Integer categoryId
    ){
        CategoryResponse<Category> response = null;
        Category idCategoryUpdate = categoryService.updateCategory(categoryRequest,categoryId);
        if (idCategoryUpdate != null){
            response = CategoryResponse.<Category>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Update Category Successfully")
                    .payload(categoryService.updateCategory(categoryRequest,categoryId))
                    .build();
        }

        return ResponseEntity.ok().body(response);
    }

}
