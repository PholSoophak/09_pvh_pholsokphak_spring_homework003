package com.example.pvhpholsokphakspringhw003.controller;

import com.example.pvhpholsokphakspringhw003.model.entity.Book;
import com.example.pvhpholsokphakspringhw003.model.request.BookRequest;
import com.example.pvhpholsokphakspringhw003.model.response.AuthorResponse;
import com.example.pvhpholsokphakspringhw003.model.response.BookResponse;
import com.example.pvhpholsokphakspringhw003.repository.BookRepository;
import com.example.pvhpholsokphakspringhw003.service.BookService;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.ibatis.annotations.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/books")
public class BookController {
    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("get-all-books")
    @Operation(summary = "Get all Books")
    public ResponseEntity<List<Book>> getAllBook() {
        return ResponseEntity.ok(bookService.getAllBook());
    }

    @GetMapping("get-books-by-id/{id}")
    @Operation(summary = "Get Book By Id")
    public ResponseEntity<Book> getBookById(@PathVariable("id") Integer bookId) {
        return ResponseEntity.ok(bookService.getBookById(bookId));
    }

    //add new book
    @PostMapping("add-new-book")
    @Operation(summary = "Add new Book")
    public ResponseEntity<BookResponse<Book>> insertNewBook(@RequestBody BookRequest bookRequest) {
        Integer bookId = bookService.insertNewBook(bookRequest);
        Book book = bookService.getBookById(bookId);
        BookResponse<Book> response = BookResponse.<Book>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .status(200)
                .message("Added New Book Successfully")
                .payload(book)
                .build();
        return ResponseEntity.ok().body(response);
    }

    @PutMapping("update-book/{id}")
    @Operation(summary = "update Book")
    public ResponseEntity<BookResponse<Book>> updateBook(@PathVariable("id") Integer id, @RequestBody BookRequest bookRequest) {
        Integer bookId = bookService.updateBook(id, bookRequest);
        Book book = bookService.getBookById(bookId);
        BookResponse<Book> response = BookResponse.<Book>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .status(200)
                .message("updated Book Successfully")
                .payload(book)
                .build();
        return ResponseEntity.ok().body(response);
    }

    //delete
    @DeleteMapping(path = "delete-book-by/{id}")
    @Operation(summary = "delete Book")
    ResponseEntity<?> deleteBookById(@PathVariable(value = "id") Integer bookId) {

        BookResponse<AuthorResponse> response = BookResponse.<AuthorResponse>builder()
                .message("Book ID: " + bookId + " Deleted Successfully")
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .status(200)
                .payload(null)
                .build();
        bookService.delete(bookId);
        return ResponseEntity.ok().body(response);
    }


}
