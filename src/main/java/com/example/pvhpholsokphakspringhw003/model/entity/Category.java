package com.example.pvhpholsokphakspringhw003.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Category {
    private Integer  categoryId;
    private String  categoryName;
}
