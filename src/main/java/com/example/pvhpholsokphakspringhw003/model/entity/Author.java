package com.example.pvhpholsokphakspringhw003.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Author {
    private Integer authorId;
    private String authorName;
    private String gender ;
}
