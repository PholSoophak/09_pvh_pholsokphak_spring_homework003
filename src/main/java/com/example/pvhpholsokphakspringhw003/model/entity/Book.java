package com.example.pvhpholsokphakspringhw003.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book {
    private Integer bookId;
    private String bookTitle;
    private LocalDateTime publishDate;
    private Author author;
    private List<Category> categories ;

}
