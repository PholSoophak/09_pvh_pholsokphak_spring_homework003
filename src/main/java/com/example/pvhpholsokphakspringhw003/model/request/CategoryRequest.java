package com.example.pvhpholsokphakspringhw003.model.request;

import lombok.AllArgsConstructor;

import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryRequest {
   private String categoryName;
}
