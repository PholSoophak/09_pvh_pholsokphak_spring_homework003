package com.example.pvhpholsokphakspringhw003.model.request;

import com.example.pvhpholsokphakspringhw003.model.entity.Author;
import com.example.pvhpholsokphakspringhw003.model.entity.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookRequest {
    private String bookTitle;
    private Integer authorId;
    private List<Integer> categoryId ;
}
