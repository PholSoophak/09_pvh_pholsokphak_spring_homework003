package com.example.pvhpholsokphakspringhw003.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookResponse<T> {
    private Timestamp timestamp;
    private String message ;
    private Integer status;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;
}
