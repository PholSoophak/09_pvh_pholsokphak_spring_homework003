package com.example.pvhpholsokphakspringhw003.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalException {
    @ExceptionHandler(EmptyException.class)
    ProblemDetail emptyHandler(EmptyException e){
        ProblemDetail detail=ProblemDetail.forStatusAndDetail(
                HttpStatus.BAD_REQUEST,
                e.getMessage()
        );
        detail.setTitle("Field empty");
        return detail;
    }
    @ExceptionHandler(NotFoundException.class)
    ProblemDetail notFoundHandler(NotFoundException e){
        ProblemDetail detail=ProblemDetail.forStatusAndDetail(
                HttpStatus.NOT_FOUND,
                e.getMessage()
        );
        detail.setTitle("NOT FOUND !");
        return detail;
    }
}
