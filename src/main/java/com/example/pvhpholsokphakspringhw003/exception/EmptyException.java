package com.example.pvhpholsokphakspringhw003.exception;

public class EmptyException extends RuntimeException{
    public EmptyException(String message) {
        super(message);
    }
}
