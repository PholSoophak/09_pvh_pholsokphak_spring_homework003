package com.example.pvhpholsokphakspringhw003;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PvhPholsokphakSpringHw003Application {

    public static void main(String[] args) {
        SpringApplication.run(PvhPholsokphakSpringHw003Application.class, args);
    }

}
