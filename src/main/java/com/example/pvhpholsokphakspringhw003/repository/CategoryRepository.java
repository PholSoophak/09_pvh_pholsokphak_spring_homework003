package com.example.pvhpholsokphakspringhw003.repository;

import com.example.pvhpholsokphakspringhw003.model.entity.Category;
import com.example.pvhpholsokphakspringhw003.model.request.AuthorRequest;
import com.example.pvhpholsokphakspringhw003.model.request.CategoryRequest;
import com.example.pvhpholsokphakspringhw003.model.response.CategoryResponse;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CategoryRepository {

    @Select("SELECT  * FROM categories_tb")
    @Results(id = "mapCategory" ,value={
            @Result(property = "categoryId",column = "category_id"),
            @Result(property = "categoryName",column = "category_name"),
    })
    List<Category> findAllCategory();

    @Select("SELECT  * FROM categories_tb where category_id =#{categoryId}")
    @ResultMap("mapCategory")
    Category getCategoryById(Integer categoryId);

    @Delete("DELETE from categories_tb where category_id =#{cId}")
    @ResultMap("mapCategory")
    boolean deleteCategoryById (@Param("cId") Integer categoryId);

    @Select("""
          insert into categories_tb(category_name) values(#{addCategory.categoryName})
          returning *
            """)
    @ResultMap("mapCategory")
    Category insertNewCategory(@Param("addCategory") CategoryRequest categoryRequest);
    @Select("""
            UPDATE categories_tb
            SET category_name=#{updateCat.categoryName}
            WHERE category_id=#{categoryId}
            RETURNING*
            """)
    @ResultMap("mapCategory")
    Category updateCategory(@Param("updateCat") CategoryRequest categoryRequest , Integer categoryId);
}
