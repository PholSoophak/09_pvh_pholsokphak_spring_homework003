package com.example.pvhpholsokphakspringhw003.repository;

import com.example.pvhpholsokphakspringhw003.model.entity.Author;
import com.example.pvhpholsokphakspringhw003.model.request.AuthorRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Mapper
public interface AuthorRepository {
    @Select("SELECT  * FROM author_tb")
    @Results(id = "mapAuthor", value = {
            @Result(property = "authorId", column = "author_id"),
            @Result(property = "authorName", column = "author_name"),
    })
    List<Author> findAllAuthor();

    @Select("SELECT * FROM author_tb WHERE author_id =#{authorId}")
    @ResultMap("mapAuthor")
    Author getAuthorById(Integer authorId);

    @Delete("DELETE FROM author_tb WHERE author_id =#{id}")
    @ResultMap("mapAuthor")
    boolean deleteAuthorById(@Param("id") Integer authorId);

    @Select("""
            INSERT INTO author_tb(author_name, gender) VALUES
            (#{addAuthor.authorName}, #{addAuthor.gender}) RETURNING author_id
            """)
    Integer insertNewAuthor(@Param("addAuthor") AuthorRequest authorRequest);

    @Select("""
            UPDATE author_tb
             SET author_name=#{update.authorName} ,
             gender=#{update.gender}
             WHERE author_id=#{authorId}
             RETURNING *
            """)
    @ResultMap("mapAuthor")
    Author updateAuthor(@Param("update")AuthorRequest authorRequest, Integer authorId);
}
