package com.example.pvhpholsokphakspringhw003.repository;

import com.example.pvhpholsokphakspringhw003.model.entity.Book;
import com.example.pvhpholsokphakspringhw003.model.entity.Category;
import com.example.pvhpholsokphakspringhw003.model.request.BookRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookRepository {
    @Select("""
            SELECT category_name ,ct.category_id FROM book_detail
            INNER JOIN categories_tb ct
            on book_detail.category_id = ct.category_id WHERE book_id=#{bookId}
            """)
    @Result(property = "categoryId", column = "category_id")
    @Result(property = "categoryName", column = "category_Name")
    List<Category> getAllCategoryByBookId(Integer bookId);

    //Get all book
    @Select("SELECT * FROM book_td")
    @Results(id = "mapBook", value = {
            @Result(property = "bookId", column = "book_id"),
            @Result(property = "bookTitle", column = "title"),
            @Result(property = "publishDate", column = "published_date"),
            @Result(property = "author", column = "author_id",
                    one = @One(select = "com.example.pvhpholsokphakspringhw003.repository.AuthorRepository.getAuthorById")),
            @Result(property = "categories", column = "book_id", many = @Many(select = "getAllCategoryByBookId"))
    })
    List<Book> findAllBook();

    //get book by id
    @Select("SELECT * FROM book_td WHERE book_id=#{bookId}")
    @ResultMap("mapBook")
    Book getBookById(Integer bookId);

    @Insert("insert into book_detail(book_id,category_id)values(#{book_id},#{category_id})")
    void addToBookDetail(Integer book_id, Integer category_id);

    @Select("""
             insert into book_td(title,author_id) values(#{addBook.bookTitle},#{addBook.authorId})
                   returning book_id
            """)
    Integer insertNewBook(@Param("addBook") BookRequest bookRequest);

    @Select("""
            update book_td set title=#{updateBook.bookTitle},author_id=#{updateBook.authorId} where book_id=#{id}
            returning book_id
            """)
    Integer updateBook(Integer id, @Param("updateBook") BookRequest bookRequest);

    @Delete("""
            delete from book_detail where book_id=#{id}
            """)
    void removeBookDetailByBookId(Integer id);

    @Delete("""
            DELETE FROM book_td WHERE book_id=#{bookId}
            """)
    Integer deleteBook (Integer bookId);


}
