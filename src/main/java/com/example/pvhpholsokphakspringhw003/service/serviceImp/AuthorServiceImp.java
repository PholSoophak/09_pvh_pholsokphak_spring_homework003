package com.example.pvhpholsokphakspringhw003.service.serviceImp;

import com.example.pvhpholsokphakspringhw003.exception.EmptyException;
import com.example.pvhpholsokphakspringhw003.exception.NotFoundException;
import com.example.pvhpholsokphakspringhw003.model.entity.Author;
import com.example.pvhpholsokphakspringhw003.model.request.AuthorRequest;
import com.example.pvhpholsokphakspringhw003.repository.AuthorRepository;
import com.example.pvhpholsokphakspringhw003.service.AuthorService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class AuthorServiceImp implements AuthorService {
    private final AuthorRepository authorRepository;

    public AuthorServiceImp(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }


    @Override
    public List<Author> getAllAuthor() {
        return authorRepository.findAllAuthor();
    }

    @Override
    public Author getAllAuthorbyId(Integer authorId) {
        if (authorRepository.getAuthorById(authorId) == null) {
            throw new NotFoundException("Author Id: " + authorId + " Not Found..!");
        }
        return authorRepository.getAuthorById(authorId);
    }

    @Override
    public boolean deleteAuthorById(Integer authorId) {
        if (authorRepository.getAuthorById(authorId) == null) {
            throw new NotFoundException("Author Id: " + authorId + " Not Found..!");
        }
        return authorRepository.deleteAuthorById(authorId);
    }
    @Override
    public Integer inseertNewAuthor(AuthorRequest authorRequest) {
        if (authorRequest.getAuthorName().isEmpty() || authorRequest.getAuthorName().isBlank() &&
                authorRequest.getGender().isEmpty() || authorRequest.getGender().isBlank()) {
            throw new EmptyException("Author Name and Gender cannot null");
        } else if (Objects.equals(authorRequest.getAuthorName(), "string")) {
            throw new EmptyException("Name cannot default String");
        } else if (Objects.equals(authorRequest.getGender(), "string")) {
            throw new EmptyException("Gender cannot default String");
        } else {
            Integer authorId = authorRepository.insertNewAuthor(authorRequest);
            return authorId;
        }

    }

    @Override
    public Author updateAuthor(AuthorRequest authorRequest, Integer authorId) {
        if(authorRepository.getAuthorById(authorId)==null){
            throw  new NotFoundException("Author Id: "+authorId+" Not Found..!");
        }
        else if (authorRequest.getAuthorName().isEmpty() || authorRequest.getAuthorName().isBlank() &&
                authorRequest.getGender().isEmpty() || authorRequest.getGender().isBlank()) {
            throw new EmptyException("Author Name and Gender cannot null");
        } else if (Objects.equals(authorRequest.getAuthorName(), "string")) {
            throw new EmptyException("Name cannot default String");
        } else if (Objects.equals(authorRequest.getGender(), "string")) {
            throw new EmptyException("Gender cannot default String");
        }
        return authorRepository.updateAuthor(authorRequest, authorId);
    }
}
