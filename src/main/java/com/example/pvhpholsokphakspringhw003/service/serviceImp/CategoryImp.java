package com.example.pvhpholsokphakspringhw003.service.serviceImp;

import com.example.pvhpholsokphakspringhw003.exception.EmptyException;
import com.example.pvhpholsokphakspringhw003.exception.NotFoundException;
import com.example.pvhpholsokphakspringhw003.model.entity.Category;
import com.example.pvhpholsokphakspringhw003.model.request.CategoryRequest;
import com.example.pvhpholsokphakspringhw003.repository.CategoryRepository;
import com.example.pvhpholsokphakspringhw003.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class CategoryImp implements CategoryService {
    private final CategoryRepository categoryRepository ;

    public CategoryImp(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAllCategory() {
        return categoryRepository.findAllCategory();
    }

    @Override
    public Category getCategoryById(Integer categoryId) {
        if(categoryRepository.getCategoryById(categoryId)==null){
            throw  new NotFoundException("Category Id: "+categoryId+" Not Found..!");
        }
        return categoryRepository.getCategoryById(categoryId);
    }

    @Override
    public boolean deleteCategoryById(Integer categoryId) {
        if(categoryRepository.getCategoryById(categoryId)==null){
            throw  new NotFoundException("Cannot Delete Category Id: "+categoryId+" Not Found..!");
        }
        return categoryRepository.deleteCategoryById(categoryId);
    }

    @Override
    public Category insertNewCategory(CategoryRequest categoryRequest) {

        if (categoryRequest.getCategoryName().isEmpty()||categoryRequest.getCategoryName().isBlank()){
            throw new EmptyException("Category Name cannot null");
        } else if (Objects.equals(categoryRequest.getCategoryName(), "string")) {
            throw new EmptyException("Name cannot default String");
        }
     return  categoryRepository.insertNewCategory(categoryRequest);
    }

    @Override
    public Category updateCategory(CategoryRequest categoryRequest, Integer categoryId) {
        if(categoryRepository.getCategoryById(categoryId)==null){
            throw  new NotFoundException("Category Id: "+categoryId+" Not Found..!");
        }
        return categoryRepository.updateCategory(categoryRequest,categoryId);
    }

}
