package com.example.pvhpholsokphakspringhw003.service.serviceImp;

import com.example.pvhpholsokphakspringhw003.exception.EmptyException;
import com.example.pvhpholsokphakspringhw003.exception.NotFoundException;
import com.example.pvhpholsokphakspringhw003.model.entity.Book;
import com.example.pvhpholsokphakspringhw003.model.request.BookRequest;
import com.example.pvhpholsokphakspringhw003.repository.BookRepository;
import com.example.pvhpholsokphakspringhw003.service.AuthorService;
import com.example.pvhpholsokphakspringhw003.service.BookService;
import com.example.pvhpholsokphakspringhw003.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class BookServiceImp implements BookService {
    private final BookRepository bookRepository;
    private final AuthorService authorService;
    private  final CategoryService categoryService;

    public BookServiceImp(BookRepository bookRepository, AuthorService authorService, CategoryService categoryService) {
        this.bookRepository = bookRepository;
        this.authorService = authorService;
        this.categoryService = categoryService;
    }

    @Override
    public List<Book> getAllBook() {
        return bookRepository.findAllBook();
    }

    @Override
    public Book getBookById(Integer bookId) {
        if(bookRepository.getBookById(bookId)==null){
            throw new NotFoundException("Book:"+bookId+" Not Found !");
        }
        return bookRepository.getBookById(bookId);
    }
    @Override
    public Integer insertNewBook(BookRequest bookRequest) {
        if (bookRequest.getBookTitle().isEmpty() || bookRequest.getBookTitle().isBlank()){
            throw new EmptyException("Book Title cannot null");
        }else if (Objects.equals(bookRequest.getBookTitle(), "string")) {
            throw new EmptyException("Book Title cannot default String");
        }
        Integer book_id=bookRepository.insertNewBook(bookRequest);
        for (int i = 0; i <bookRequest.getCategoryId().size() ; i++) {
            bookRepository.addToBookDetail(book_id,bookRequest.getCategoryId().get(i));
        }
        return book_id;
    }

    @Override
    public Integer updateBook(Integer id, BookRequest bookRequest) {
        if(bookRepository.getBookById(id)==null){
            throw new NotFoundException("Book:"+id+" Not Found !");
        }else if (bookRequest.getBookTitle().isEmpty() || bookRequest.getBookTitle().isBlank()){
            throw new EmptyException("Book Title cannot null");
        }else if (Objects.equals(bookRequest.getBookTitle(), "string")) {
            throw new EmptyException("Name cannot default String");
        } else {
            authorService.getAllAuthorbyId(bookRequest.getAuthorId());
            Integer book_id=bookRepository.updateBook(id,bookRequest);

            for (int i = 0; i <bookRequest.getCategoryId().size() ; i++) {
                categoryService.getCategoryById(bookRequest.getCategoryId().get(i));
            }
            bookRepository.removeBookDetailByBookId(book_id);
            for (int i = 0; i <bookRequest.getCategoryId().size() ; i++) {
              bookRepository.addToBookDetail(book_id, bookRequest.getCategoryId().get(i));
            }
            return book_id;
        }

    }

    public void delete(Integer id) {
        if(bookRepository.getBookById(id)==null){
            throw new NotFoundException("Book:"+id+" Not Found !");
        }
        Book book = getBookById(id);
        bookRepository.deleteBook(book.getBookId());
    }

}
