package com.example.pvhpholsokphakspringhw003.service;

import com.example.pvhpholsokphakspringhw003.model.entity.Author;
import com.example.pvhpholsokphakspringhw003.model.entity.Category;
import com.example.pvhpholsokphakspringhw003.model.request.AuthorRequest;
import com.example.pvhpholsokphakspringhw003.model.request.CategoryRequest;
import com.example.pvhpholsokphakspringhw003.model.response.CategoryResponse;

import java.util.List;


public interface CategoryService {
        List<Category> getAllCategory();
        Category getCategoryById (Integer categoryId);

        boolean deleteCategoryById(Integer categoryId);

        Category insertNewCategory (CategoryRequest categoryRequest);

        Category updateCategory(CategoryRequest categoryRequest, Integer categoryId);
}
