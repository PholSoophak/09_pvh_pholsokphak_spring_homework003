package com.example.pvhpholsokphakspringhw003.service;

import com.example.pvhpholsokphakspringhw003.model.entity.Author;
import com.example.pvhpholsokphakspringhw003.model.request.AuthorRequest;

import java.util.List;

public interface AuthorService {
    List  <Author> getAllAuthor();
    Author getAllAuthorbyId(Integer authorId);

    boolean deleteAuthorById (Integer authorId);

    Integer inseertNewAuthor(AuthorRequest authorRequest);

    Author updateAuthor(AuthorRequest authorRequest ,Integer authorId);
}
