package com.example.pvhpholsokphakspringhw003.service;

import com.example.pvhpholsokphakspringhw003.model.entity.Book;
import com.example.pvhpholsokphakspringhw003.model.request.BookRequest;

import java.util.List;

public interface BookService {
    List<Book> getAllBook();
    Book getBookById(Integer bookId);

    Integer insertNewBook(BookRequest bookRequest);

    Integer updateBook(Integer id, BookRequest bookRequest);

    void delete(Integer id);
}
